import telebot
import os
import django
from telebot import types
from myoperation import iloscon, iloswage
import json

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "web2finance.settings")
django.setup()

from interfinance.models import Userecon, Log, Shop_lot, Auc_lot

telebot.apihelper.proxy = {'https': 'socks5://geek:socks@t.geekclass.ru:7777'}

token = "951392946:AAGm1QX-xZVN8R1oBwIAMPA3rt3OiwN1_rc"
bot = telebot.TeleBot(token=token)

goods = Shop_lot.objects.all()

# def find_telegram_by_id(id):
#     try:
#         with open('base.json') as file:
#             base = json.loads(file.read())
#         if id in base:
#             return base[id]
#         return None
#     except:
#         return None

k = 0


@bot.message_handler(commands=['start'])
def main(message):
    bot.send_message(
        message.chat.id,
        '''Начнём...''',
        reply_markup=get_keyboard_add())


def get_keyboard_add():
    btns = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    keyboard_new = types.KeyboardButton("Создать счёт")
    keyboard_redirect = types.KeyboardButton("Функции")
    btns.add(keyboard_new, keyboard_redirect)
    return btns


def keyboard_func():
    btns_func = types.ReplyKeyboardMarkup(one_time_keyboard=False, resize_keyboard=True)
    keyboard_balance = types.KeyboardButton("Баланс")
    keyboard_supp = types.KeyboardButton("Начислить")
    keyboard_log = types.KeyboardButton("История")
    keyboard_exchange = types.KeyboardButton("Обмен")
    keyboard_shop = types.KeyboardButton("Магазин")
    btns_func.add(keyboard_shop)
    btns_func.add(keyboard_balance, keyboard_supp)
    btns_func.add(keyboard_log, keyboard_exchange)
    return btns_func


def shop():
    global offset
    global k
    keyboard = types.ReplyKeyboardMarkup()
    global goods
    name = Shop_lot.title_s
    price = Shop_lot.price_s
    # pk = find_telegram_by_id(Shop_lot.aiid)
    offset = Shop_lot.objects.all()
    button1 = types.KeyboardButton(text="{} ({} IL".format(offset[0+k].title_s, offset[0+k].price_s),
                                           callback_data="buy_{0}".format(0+k))
    button2 = types.KeyboardButton(text="{} ({} IL".format(offset[1+k].title_s, offset[1+k].price_s),
                                           callback_data="buy_{0}".format(1+k))
    button3 = types.KeyboardButton(text="{} ({} IL".format(offset[2+k].title_s, offset[2+k].price_s),
                                           callback_data="buy_{0}".format(2+k))
    button4 = types.KeyboardButton(text="{} ({} IL".format(offset[3+k].title_s, offset[3+k].price_s),
                                           callback_data="buy_{0}".format(3+k))
    button_back = types.KeyboardButton(text="Назад", callback_data="back")

    button_forward = types.KeyboardButton(text="Вперёд", callback_data="forward")

    keyboard.add(button1)
    keyboard.add(button2)
    keyboard.add(button3)
    keyboard.add(button4)
    keyboard.add(button_back)
    keyboard.add(button_forward)
    return keyboard


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    global k
    global goods
    if call.data == "back":
        k -= 4
        if k < 0:
            k = 0
        shop()
    elif call.data == "forward":
        k += 4
        if k > (goods.objects.count() - 4):
            k = goods.objects.count() - 4
        shop()
    elif call.data.startswith("buy"):
        item = call.data.split('_')[1]

        shop()


@bot.message_handler(content_types=['text'])
def refresh_base(message):
    if message.text == "Создать счёт":
        username = message.from_user.username
        idu = message.chat.id
        newecon = Userecon()
        newecon.teleusername = username
        newecon.teleuser_id = idu
        newecon.save()
        bot.reply_to(message, text="Ваш кошелёк успешно создан")
    elif message.text == "Функции":
        bot.send_message(
            message.chat.id,
            '''Функции''',
            reply_markup=keyboard_func())
    elif message.text == "Баланс":
        username = message.from_user.username
        a = iloscon(username)
        bot.send_message(message.chat.id, text="{} IL".format(a.ilos))
    elif message.text == "Магазин":
        bot.send_message(
            message.chat.id,
            '''За покупками''',
            reply_markup=shop())
    elif message.text == "Начисление":
        pass
    elif message.text == "История":
        username = message.from_user.username
        userecon_obj = Userecon.objects.get(teleusername=username)
        log = Log.objects.filter(teleuser=userecon_obj)
        for l in log:
            bot.send_message(message.chat.id, text="{}, {}, {}".format(l.operation, l.sumilos, l.date))
        reply_markup = keyboard_func()


bot.polling()
