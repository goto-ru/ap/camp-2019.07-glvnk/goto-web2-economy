from django.contrib import admin
from .models import Userecon, Log, Shop_lot, Auc_lot

admin.site.register(Userecon)
admin.site.register(Log)
admin.site.register(Shop_lot)
admin.site.register(Auc_lot)