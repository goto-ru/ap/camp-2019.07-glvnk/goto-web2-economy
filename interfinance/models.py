from django.db import models


class Userecon(models.Model):
    teleusername = models.TextField()
    teleuser_id = models.IntegerField(default=1)
    ilos = models.IntegerField(default=0)
    check = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    wage = models.IntegerField(default=0)


class Shop_lot(models.Model):
    aiid = models.AutoField(primary_key=True)
    title_s = models.TextField()
    price_s = models.IntegerField()


class Auc_lot(models.Model):
    aiid = models.AutoField(primary_key=True)
    title_a = models.TextField()
    price_a = models.IntegerField()


class Log(models.Model):
    teleuser = models.ForeignKey(Userecon, on_delete=models.CASCADE, related_name="user_log")
    operation = models.TextField()
    date = models.DateField(auto_now_add=True)
    sumilos = models.IntegerField()
