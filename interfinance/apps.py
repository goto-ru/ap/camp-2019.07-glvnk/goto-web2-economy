from django.apps import AppConfig


class InterfinanceConfig(AppConfig):
    name = 'interfinance'
