# Generated by Django 2.2.3 on 2019-07-27 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interfinance', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='auc_lot',
            name='id',
        ),
        migrations.RemoveField(
            model_name='shop_lot',
            name='id',
        ),
        migrations.AddField(
            model_name='auc_lot',
            name='aiid',
            field=models.AutoField(default=1, primary_key=True, serialize=False),
        ),
        migrations.AddField(
            model_name='shop_lot',
            name='aiid',
            field=models.AutoField(default=1, primary_key=True, serialize=False),
        ),
    ]
